FROM amazonlinux:latest
RUN yum install -y tar zip unzip passwd java-1.8.0-openjdk-devel  hostname pip procps iputils nc python39 nano net-tools lsof  openssl openssh-server openssh-clients htop
RUN pip install numpy pandas jupyter findspark koalas
RUN  mkdir /entrypoint
COPY entrypoint.sh /entrypoint
RUN  mkdir /opt/spark
RUN  mkdir /opt/pdi


COPY pdi-ce-9.2.0.0-290.zip /tmp/pdi.zip
RUN  unzip /tmp/pdi.zip -d /opt/pdi
RUN  rm -rf  /tmp/pdi.zip
#RUN curl -o /tmp/spark.tgz https://dlcdn.apache.org/spark/spark-3.2.1/spark-3.2.1-bin-hadoop3.2.tgz



#COPY spark-3.2.1-bin-hadoop3.2.tgz /tmp/spark.tgz
#RUN  tar -xf /tmp/spark.tgz -C /opt/spark
#RUN  rm -rf /tmp/spark.tgz

COPY spark-2.4.0-bin-hadoop2.7.tgz /tmp/spark.tgz
RUN  tar -xf /tmp/spark.tgz -C /opt/spark
RUN  rm -rf /tmp/spark.tgz

#RUN curl -o /opt/spark/spark-3.2.1-bin-hadoop3.2/jars/postgresql-42.3.3.jar https://jdbc.postgresql.org/download/postgresql-42.3.3.jar
COPY postgresql-42.3.3.jar /opt/spark/spark-2.4.0-bin-hadoop2.7/jars/postgresql-42.3.3.jar
COPY mysql-connector-java-8.0.28.jar /opt/spark/spark-2.4.0-bin-hadoop2.7/jars/mysql-connector-java-8.0.28.jar
COPY mongo-spark-connector_2.12-3.0.1.jar /opt/spark/spark-2.4.0-bin-hadoop2.7/jars/mongo-spark-connector_2.12-3.0.1.jar
COPY mongo-java-driver-3.12.10.jar /opt/spark/spark-2.4.0-bin-hadoop2.7/jars/mongo-java-driver-3.12.10.jar
#https://repo1.maven.org/maven2/org/mongodb/spark/mongo-spark-connector_2.12/3.0.1/mongo-spark-connector_2.12-3.0.1.jar
RUN  mkdir /dadosExemplos
COPY dadosExemplos/ /dadosExemplos/
#findspark faz a integração entre o spark e o jupyter

RUN ssh-keygen -t ecdsa -f /etc/ssh/ssh_host_ed25519_key
RUN ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key
RUN ssh-keygen -t dsa -f /etc/ssh/ssh_host_dsa_key
RUN ssh-keygen -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key
#RUN useradd -rm -d /home/admin -s /bin/bash -g root admin -p "$(openssl passwd -1 admin)"
RUN echo 'root' | passwd --stdin root
ENV SPARK_HOME "/opt/spark/spark-2.4.0-bin-hadoop2.7"
ENV JAVA_HOME "/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.312.b07-1.amzn2.0.2.x86_64/"
ENV PATH "${SPARK_HOME}/bin:${SPARK_HOME}/sbin:${JAVA_HOME}/bin:${PATH}"

ENV PENTAHO_HOME "C:\Pentaho"
ENV PENTAHO_INSTALLED_LICENSE_PATH "C:\Pentaho\.installedLicenses.xml"
ENV PENTAHO_JAVA_HOME "C:\Pentaho\java"

ENTRYPOINT ["/entrypoint/entrypoint.sh"]

