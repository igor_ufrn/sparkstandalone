#!/bin/sh

whereis java

if [ "$MASTER" = true ] ; then
    echo 'INICIANDO MASTER'
    start-master.sh
else
    echo 'INICIANDO SLAVE'
    start-slave.sh spark://$HOST_SPARK_MASTER:7077
fi
/usr/sbin/sshd
mkdir /notebookDir 
jupyter notebook -y  --port=8888 --no-browser --ip=0.0.0.0 --allow-root  --NotebookApp.token='' --NotebookApp.password='' --notebook-dir /notebookDir 
/bin/sh